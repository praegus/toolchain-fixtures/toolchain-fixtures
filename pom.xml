<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>nl.praegus</groupId>
    <artifactId>toolchain-fixtures</artifactId>
    <url>https://gitlab.com/praegus/toolchain-fixtures/toolchain-fixtures</url>
    <name>Praegus Toolchain extended fixture pack</name>
    <description>Extensions on existing fixtures to aid in automated testing</description>
    <version>1.15-SNAPSHOT</version>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>net.bytebuddy</groupId>
                <artifactId>byte-buddy</artifactId>
                <version>1.9.10</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.fitnesse</groupId>
            <artifactId>fitnesse</artifactId>
            <version>20190628</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>nl.hsac</groupId>
            <artifactId>hsac-fitnesse-fixtures</artifactId>
            <version>4.5.0</version>
            <scope>provided</scope>
        </dependency>
        <!-- SSH -->
        <dependency>
            <groupId>com.jcraft</groupId>
            <artifactId>jsch</artifactId>
            <version>0.1.55</version>
        </dependency>
        <!-- XHR -->
        <dependency>
            <groupId>net.lightbody.bmp</groupId>
            <artifactId>browsermob-core</artifactId>
            <version>2.1.5</version>
            <exclusions>
                <exclusion>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-core</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-annotations</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.apache.commons</groupId>
                    <artifactId>commons-lang3</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <!-- Gmail -->
        <dependency>
            <groupId>com.google.api-client</groupId>
            <artifactId>google-api-client</artifactId>
            <version>1.28.0</version>
            <exclusions>
                <exclusion>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.google.oauth-client</groupId>
            <artifactId>google-oauth-client-jetty</artifactId>
            <version>1.28.0</version>
        </dependency>
        <dependency>
            <groupId>com.google.apis</groupId>
            <artifactId>google-api-services-gmail</artifactId>
            <version>v1-rev20190422-1.28.0</version>
        </dependency>

        <dependency>
            <groupId>javax.mail</groupId>
            <artifactId>mail</artifactId>
            <version>1.5.0-b01</version>
        </dependency>

        <!-- PDF Compare -->
        <dependency>
            <groupId>de.redsix</groupId>
            <artifactId>pdfcompare</artifactId>
            <version>1.1.40</version>
        </dependency>


        <!-- for unittest -->
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>3.12.2</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>2.27.0</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <compilerArgument>-parameters</compilerArgument>
                    <source>8</source>
                    <target>8</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
                <configuration>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <useReleaseProfile>false</useReleaseProfile>
                    <releaseProfiles>release</releaseProfiles>
                    <goals>deploy</goals>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.7</version>
                <extensions>true</extensions>
                <configuration>
                    <serverId>staging</serverId>
                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>true</autoReleaseAfterClose>
                </configuration>
                <goals>
                    <goal>deploy</goal>
                </goals>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.3</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>generate-code-coverage-report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>


            <!-- Aggregate JavaDoc for use with autoCompleteResponder -->
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
                <extensions>true</extensions>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>javadoc</goal>
                        </goals>
                    </execution>
                </executions>

                <configuration>
                    <doclet>nl.praegus.doclets.FixtureDocGenerator</doclet>
                    <docletArtifact>
                        <groupId>nl.praegus</groupId>
                        <artifactId>fixture-doc-generator</artifactId>
                        <version>1.0</version>
                    </docletArtifact>
                    <excludePackageNames>nl.hsac.*</excludePackageNames>
                    <includeDependencySources>true</includeDependencySources>
                    <dependencySourceIncludes>
                        <dependencySourceInclude>nl.hsac:*</dependencySourceInclude>--&gt;
                    </dependencySourceIncludes>
                    <!-- AutoCompleteResponder expects the documentation in wiki/fixtures -->
                    <reportOutputDirectory>${project.basedir}/src/main/resources/META-INF/</reportOutputDirectory>
                    <useStandardDocletOptions>false</useStandardDocletOptions>
                </configuration>
            </plugin>

        </plugins>


    </build>

    <packaging>jar</packaging>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
            <comments>A business-friendly OSS license</comments>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Tom Heintzberger</name>
            <email>tom.heintzberger@praegus.nl</email>
            <organization>Praegus Solutions B.V.</organization>
            <organizationUrl>https://praegus.nl/solutions/</organizationUrl>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git@gitlab.com:praegus/toolchain-fixtures/toolchain-fixtures.git</connection>
        <url>https://gitlab.com/praegus/toolchain-fixtures/toolchain-fixtures.git</url>
        <developerConnection>scm:git:git@gitlab.com:praegus/toolchain-fixtures/toolchain-fixtures.git
        </developerConnection>
        <tag>HEAD</tag>
    </scm>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>2.4</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>3.1.0</version>
                        <configuration>
                            <tags>
                                <tag>
                                    <name>wiki</name>
                                    <placement>m</placement>
                                    <head>Wiki usage:</head>
                                </tag>
                            </tags>
                        </configuration>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>

                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>package</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <distributionManagement>
        <snapshotRepository>
            <id>snapshots</id>
            <name>Maven Central Snapshot Repository</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>staging</id>
            <name>Maven Central Staging Repository</name>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2</url>
        </repository>
    </distributionManagement>
</project>